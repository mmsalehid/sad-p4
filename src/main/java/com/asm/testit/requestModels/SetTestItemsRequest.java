package com.asm.testit.requestModels;

import java.util.*;
public class SetTestItemsRequest {
    private List<String > testItems;

    public List<String> getTestItems() {
        return testItems;
    }

    public void setTestItems(List<String> testItems) {
        this.testItems = testItems;
    }
}
