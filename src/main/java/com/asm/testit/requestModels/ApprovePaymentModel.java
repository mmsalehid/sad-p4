package com.asm.testit.requestModels;

import com.asm.testit.models.enumerations.BankPortal;

public class ApprovePaymentModel {
    private BankPortal portal;
    private int paymentId;

    public BankPortal getPortal() {
        return portal;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPortal(BankPortal portal) {
        this.portal = portal;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }
}
