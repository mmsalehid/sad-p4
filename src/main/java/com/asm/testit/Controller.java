package com.asm.testit;

import com.asm.testit.models.*;
import com.asm.testit.models.ResponseModels.LabWithPriceInfo;
import com.asm.testit.models.ResponseModels.PaymentForm;
import com.asm.testit.models.enumerations.BankPortal;
import com.asm.testit.models.enumerations.BloodGroup;
import com.asm.testit.models.enumerations.Gender;
import com.asm.testit.models.enumerations.KitType;
import com.asm.testit.requestModels.ApprovePaymentModel;
import com.asm.testit.requestModels.SetTestItemsRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.CallableMethodReturnValueHandler;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.*;

@RestController
public class Controller {
    private List<Laboratory> labratories;
    private List<SamplerSlot> slots;
    private List<TestItem> testItems;
    private List<Insurance> insurances;
    private Patient patient;
    private int paymentId;

    public Controller(){
        labratories = new ArrayList<>();
        slots = new ArrayList<>();
        testItems = new ArrayList<>();
        insurances = new ArrayList<>();
        initData();
        paymentId = 0;

    }
    @RequestMapping(value = "/init_test_request",method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK, reason = "request successfully submitted!")
    public void initTestRequest(@RequestParam Map<String,String> queryParams){
        boolean useIns = false;
        if(queryParams.containsKey("useInsurance")){
            String useInsurance = queryParams.get("useInsurance");
            if (useInsurance.equals("true")){
                useIns = true;
            }
            else if(useInsurance.equals("false")){
                useIns = false;
            }
        }
        Test newTest = new Test(useIns);
        patient.initiateRequest(newTest);
        if (useIns && !checkInsurance(useIns)){
            throw new RuntimeException("بیمه ی کاربر تعریف شده نیست.");
        }
        System.out.println(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault())
                .toInstant()));
    }

    @RequestMapping(value = "/set_test_items", method = RequestMethod.POST)
    public List<LabWithPriceInfo> setTestItems(@RequestBody SetTestItemsRequest setTestItemsRequest){
        List<String> testTypes= setTestItemsRequest.getTestItems();
        List<TestItem> testTestItems = new ArrayList<>();
        for (String testType: testTypes){
            boolean existFlag = false;
            for (TestItem testItem: testItems){
                if (testItem.getType().equals(testType)){
                    existFlag = true;
                    testTestItems.add(testItem);
                }
            }
            if (!existFlag){
                throw new RuntimeException("آزمایش مورد نظر وجود ندارد!");
            }
        }
        patient.setTestItems(testTestItems);
        List<LabWithPriceInfo> priceInfo = new ArrayList<>();
        for(Laboratory lab: labratories){
            System.out.println("TEST3");
            if (lab.checkTestItems(testTypes)){
                System.out.println("TEST1");
                if(patient.getCurrTestUseIns()){
                    System.out.println("TEST2");
                    if(lab.checkInsurance(patient.getInsuranceName())){
                        LabWithPriceInfo pi = calculateTestItemsPrices(testTestItems,lab);
                        Insurance patientIns = findInsurance();
                        pi.setFinalPriceOff(pi.getFinalPrice()- new Float(pi.getFinalPrice()*patientIns.getOffFactor()).intValue());
                        priceInfo.add(pi);
                    }
                }
                else{
                    LabWithPriceInfo pi = calculateTestItemsPrices(testTestItems,lab);
                    Insurance patientIns = findInsurance();
                    pi.setFinalPriceOff(pi.getFinalPrice());
                    priceInfo.add(pi);
                }
            }
        }

        return priceInfo;
    }

    @RequestMapping(value = "/choose_lab", method = RequestMethod.POST)
    public List<SamplerSlot> chooseLab(@RequestBody String chosenLab){
        System.out.println(chosenLab);
        Laboratory chosenLaboratory = null;
        for (Laboratory lab:labratories){
            if(lab.getName().equals(chosenLab)){
                chosenLaboratory = lab;
            }
        }

        patient.assignLabToCurrTest(chosenLaboratory);
        patient.calculatePriceForCurrentTest();
        return findProperSlots();
    }

    @RequestMapping(value = "/choose_slot", method = RequestMethod.POST)
    public SamplerSlot chooseSlot(@RequestBody String slotId){
        int id = Integer.valueOf(slotId);
        SamplerSlot chosenSlot = null;
        for(SamplerSlot ss: slots){
            if (ss.getId() == id){
                chosenSlot = ss;
            }
        }
        if (chosenSlot == null) {
            throw new  RuntimeException("اسلات مورد نظر وجود ندارد.");
        }
        patient.setSlotForCurrentTest(chosenSlot);
        return chosenSlot;
    }

    @RequestMapping(value = "/show_payment_details", method = RequestMethod.GET)
    public LabWithPriceInfo showPaymentDetails(){
        LabWithPriceInfo priceInfo = patient.getCurrentTestPriceInfo();
        return priceInfo;
    }

    @RequestMapping(value = "/accept_payment", method = RequestMethod.GET)
    public BankPortal[] acceptPayment(){
        return BankPortal.values();
    }

    @RequestMapping(value = "/choose_portal" , method = RequestMethod.POST)
    public PaymentForm choosePortal(@RequestBody String portalName){
        PaymentForm newPayment = new PaymentForm();
        newPayment.setId(paymentId);
        paymentId++;
        newPayment.setPortal(BankPortal.valueOf(portalName));
        newPayment.setPrice(patient.getCurrentTestPriceInfo().getFinalPrice());
        return newPayment;
    }

    @RequestMapping(value = "/approve_payment", method = RequestMethod.POST)
    public Payment approvePayment(@RequestBody ApprovePaymentModel approvePaymentModel){
        Date today = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault())
                .toInstant());
        PaymentInfo info = new PaymentInfo(approvePaymentModel.getPaymentId(),today,patient.getCurrentTestPriceInfo().getFinalPriceOff(),true);
        Payment payment = new Payment(approvePaymentModel.getPortal(),info);
        patient.setPaymentForCurrentTest(payment);
        patient.bookChosenSlot();
        return payment;
    }

    private Insurance findInsurance(){
        String patientInsurance = patient.getInsuranceName();
        Insurance findedIns = null;
        for (Insurance ins: this.insurances){
            if (ins.getName().equals(patientInsurance)){
                findedIns = ins;
            }
        }
        return findedIns;
    }

    private boolean checkInsurance(boolean useIns){
        Insurance findedIns = findInsurance();
        if (findedIns == null){
            return false;
        }
        if(useIns){
            patient.setInsuranceForCurrentTest(findedIns);
        }
        return findedIns.checkPatient(patient.getnId());
    }

    private List<SamplerSlot> findProperSlots(){
        Date today = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault())
                .toInstant());
        List<SamplerSlot> offerredSlots = new ArrayList<>();
        for (SamplerSlot ss: slots)
        {
            if (ss.getStartDate().after(today) && ss.isAvailable()){
                offerredSlots.add(ss);
            }
        }
        return offerredSlots;
    }

    private LabWithPriceInfo calculateTestItemsPrices(List<TestItem> items, Laboratory lab){
        LabWithPriceInfo priceInfo = new LabWithPriceInfo();
        Map<String,Integer> prices = new HashMap<>();
        int totalPrice = 0;
        for (TestItem item: items){
            Float price = item.getPriceFactor() * lab.getBasePrice();
            prices.put(item.getType(),price.intValue());
            totalPrice += price.intValue();
        }
        priceInfo.setTestItemPrices(prices);
        priceInfo.setFinalPrice(totalPrice);
        priceInfo.setLabName(lab.getName());
        return priceInfo;
    }

    private void initData(){
        this.patient = new Patient("test","test", "test", "test1", BloodGroup.AB_P, Gender.MAN,"","ins1");
        TestItem ti1 = new TestItem("testItem1", KitType.valueOf("KIT1"),1.2f);
        TestItem ti2 = new TestItem("testItem2", KitType.valueOf("KIT2"),1.5f);
        TestItem ti3 = new TestItem("testItem3", KitType.valueOf("KIT3"),1.8f);
        List<String> ins1List = new ArrayList<>();
        ins1List.add("test1");
        List<String> ins2List = new ArrayList<>();
        ins2List.add("test2");
        List<String> ins3List = new ArrayList<>();
        ins3List.add("test3");
        Insurance ins1 = new Insurance("ins1", 0.2f, ins1List);
        Insurance ins2 = new Insurance("ins2", 0.2f, ins2List);
        Insurance ins3 = new Insurance("ins3", 0.2f, ins3List);
        List<String> lab1InsList = new ArrayList<>();
        lab1InsList.add("ins1");
        List<String> lab2InsList = new ArrayList<>();
        lab2InsList.add("ins2");
        List<String> lab3InsList = new ArrayList<>();
        lab3InsList.add("ins1");
        List<TestItem> lab1TestItems = new ArrayList<>();
        lab1TestItems.add(ti1);
        lab1TestItems.add(ti3);
        List<TestItem> lab2TestItems = new ArrayList<>();
        lab2TestItems.add(ti3);
        lab2TestItems.add(ti2);
        List<TestItem> lab3TestItems = new ArrayList<>();
        lab3TestItems.add(ti3);
        lab3TestItems.add(ti2);
        Laboratory lab1 = new Laboratory("lab1", 12000, "lab1address",lab1TestItems,lab1InsList);
        Laboratory lab2 = new Laboratory("lab2", 15000, "lab2address",lab2TestItems,lab2InsList);
        Laboratory lab3 = new Laboratory("lab3", 10000, "lab3address",lab3TestItems,lab3InsList);
        labratories.add(lab1);
        labratories.add(lab2);
        labratories.add(lab3);
        testItems.add(ti1);
        testItems.add(ti2);
        testItems.add(ti3);
        insurances.add(ins1);
        insurances.add(ins2);
        insurances.add(ins3);
        Calendar cal1 =  new GregorianCalendar(2020, Calendar.JULY, 22);
        cal1.set(Calendar.HOUR_OF_DAY,17);
        cal1.set(Calendar.MINUTE,30);
        Calendar cal2 =  new GregorianCalendar(2020, Calendar.JULY, 22);
        cal2.set(Calendar.HOUR_OF_DAY,18);
        cal2.set(Calendar.MINUTE,0);
        SamplerSlot ss = new SamplerSlot(1,cal1.getTime(),cal2.getTime());
        Calendar cal3 =  new GregorianCalendar(2020, Calendar.JULY, 23);
        cal3.set(Calendar.HOUR_OF_DAY,17);
        cal3.set(Calendar.MINUTE,30);
        Calendar cal4 =  new GregorianCalendar(2020, Calendar.JULY, 23);
        cal4.set(Calendar.HOUR_OF_DAY,18);
        cal4.set(Calendar.MINUTE,0);
        SamplerSlot ss1 = new SamplerSlot(2,cal3.getTime(),cal4.getTime());
        slots.add(ss);
        slots.add(ss1);
    }


}
