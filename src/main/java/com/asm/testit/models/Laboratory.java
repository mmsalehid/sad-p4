package com.asm.testit.models;


import java.util.*;

public class Laboratory
{
    private String name;
    private int basePrice;
    private String address;
    private List<TestItem> testItemList;
    private List<String> insuranceNameList;

    public Laboratory(String name, int basePrice, String address, List<TestItem> items, List<String> insuranceNameList){
        this.name = name;
        this.basePrice = basePrice;
        this.address = address;
        this.insuranceNameList = insuranceNameList;
        this.testItemList = items;
    }

    public boolean checkTestItems(List<String> items){
        for (String item:items){
            boolean existFlag = false;
            for (TestItem testItem: testItemList){
                if(testItem.getType().equals(item)){
                    existFlag = true;
                }
            }
            if (!existFlag)
                return false;
        }
        return true;

    }

    public boolean checkInsurance(String insuranceName){
        for (String insName: insuranceNameList){
            if(insName.equals(insuranceName)){
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public int getBasePrice() {
        return basePrice;
    }

}
