package com.asm.testit.models;

import com.asm.testit.models.ResponseModels.LabWithPriceInfo;

import java.util.*;

public class Test {
    private String testId;
    private int price;
    private int priceOff;
    private boolean useInsurance;
    private List<TestItem> items;
    private Map<String,Integer> itemPrices;
    private boolean insuranceApprove;
    private Laboratory operatingLab;
    private boolean isRegistered;
    private Payment payment;
    private SamplerSlot samplerSlot;
    Insurance insurance;

    public Test(boolean useInsurance){
        itemPrices = new HashMap<>();
        this.useInsurance = useInsurance;
        this.isRegistered = false;
    }

    public void setItems(List<TestItem> items) {
        this.items = items;
    }

    public boolean isUseInsurance() {
        return useInsurance;
    }

    public void assignLaboratory(Laboratory lab){
        operatingLab = lab;
    }

    public void setSamplerSlot(SamplerSlot samplerSlot) {
        this.samplerSlot = samplerSlot;
    }

    public int getPrice() {
        return price;
    }

    public int getPriceOff() {
        return priceOff;
    }

    public Laboratory getOperatingLab() {
        return operatingLab;
    }

    public List<TestItem> getItems() {
        return items;
    }

    public String getTestId() {
        return testId;
    }

    public Payment getPayment() {
        return payment;
    }

    public SamplerSlot getSamplerSlot() {
        return samplerSlot;
    }

    public Insurance getInsurance() {
        return insurance;
    }

    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }

    public void calculatePrice(){
        int totalPrice = 0;
        for (TestItem item: items){
            System.out.println(item.getPriceFactor());
            System.out.println(operatingLab.getBasePrice());
            Float price = new Float(item.getPriceFactor() * operatingLab.getBasePrice());
            itemPrices.put(item.getType(),price.intValue());
            totalPrice += price.intValue();
        }
        price = totalPrice;
        if (useInsurance){
            priceOff = price- new Float(price*insurance.getOffFactor()).intValue();
        }
        else
            priceOff = price;

    }

    public LabWithPriceInfo getPriceInfo(){
        LabWithPriceInfo priceInfo = new LabWithPriceInfo();
        priceInfo.setTestItemPrices(itemPrices);
        priceInfo.setLabName(operatingLab.getName());
        priceInfo.setFinalPrice(price);
        priceInfo.setFinalPriceOff(priceOff);
        return priceInfo;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public void bookChosenSlot(){
        this.samplerSlot.book();
    }
}
