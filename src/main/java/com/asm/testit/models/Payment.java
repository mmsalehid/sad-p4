package com.asm.testit.models;

import com.asm.testit.models.enumerations.BankPortal;

public class Payment {
    private BankPortal portal;
    private PaymentInfo info;

    public Payment(BankPortal portal,PaymentInfo paymentInfo){
        this.portal = portal;
        this.info = paymentInfo;
    }

    public Payment(){}

    public BankPortal getPortal() {
        return portal;
    }

    public PaymentInfo getInfo() {
        return info;
    }
}
