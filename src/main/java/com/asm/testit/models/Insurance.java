package com.asm.testit.models;

import java.util.*;

public class Insurance {
    private String name;
    private float offFactor;
    private List<String> insurredList;

    public Insurance(String name, float offFactor, List<String> insurredList){
        this.name = name;
        this.offFactor = offFactor;
        this.insurredList = insurredList;
    }

    public String getName() {
        return name;
    }

    public boolean checkPatient(String nid){
        for (String id: insurredList){
            if(id.equals(nid))
                return true;
        }
        return false;
    }

    public float getOffFactor() {
        return offFactor;
    }
}
