package com.asm.testit.models;

import java.util.Date;

public class SamplerSlot {
    private int id;
    private Date startDate;
    private Date endDate;
    private boolean isAvailable;

    public SamplerSlot(int id, Date startDate, Date endDate){
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        isAvailable = true;
    }

    public int getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void book(){
        this.isAvailable = false;
    }
}
