package com.asm.testit.models;

public class User {
    private String name;
    private String phoneNumber;
    private String password;
    private String nId;

    public User(String name, String phoneNumber, String password, String nId){
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.nId = nId;
        this.password = password;
    }

    public String getnId() {
        return nId;
    }
}
