package com.asm.testit.models;

import com.asm.testit.models.enumerations.KitType;

public class TestItem {
    private String type;
    private KitType kitType;
    private float priceFactor;

    public TestItem(String type, KitType kitType, float priceFactor){
        this.type = type;
        this.kitType = kitType;
        this.priceFactor = priceFactor;
    }

    public String getType() {
        return type;
    }

    public float getPriceFactor() {
        return priceFactor;
    }
}
