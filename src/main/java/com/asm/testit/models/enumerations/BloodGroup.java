package com.asm.testit.models.enumerations;

public enum  BloodGroup {
    A_P,A_N,B_P,B_N,AB_P,AB_N,O_P,O_N
}
