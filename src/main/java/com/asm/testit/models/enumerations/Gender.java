package com.asm.testit.models.enumerations;

public enum Gender {
    MAN, WOMAN
}
