package com.asm.testit.models;

import java.util.Date;

public class PaymentInfo {
    private int id;
    private Date date;
    private int price;
    private boolean isApproved;

    public PaymentInfo(int id,Date date, int price, boolean isApproved){
        this.id = id;
        this.date = date;
        this.price = price;
        this.isApproved = isApproved;
    }

    public PaymentInfo(){}

    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public Date getDate() {
        return date;
    }
}
