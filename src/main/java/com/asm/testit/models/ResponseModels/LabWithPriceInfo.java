package com.asm.testit.models.ResponseModels;

import java.util.Map;

public class LabWithPriceInfo {
    private String labName;
    private Map<String,Integer> testItemPrices;
    private int finalPrice;
    private int finalPriceOff;

    public int getFinalPrice() {
        return finalPrice;
    }

    public int getFinalPriceOff() {
        return finalPriceOff;
    }

    public Map<String, Integer> getTestItemPrices() {
        return testItemPrices;
    }

    public String getLabName() {
        return labName;
    }

    public void setFinalPrice(int finalPrice) {
        this.finalPrice = finalPrice;
    }

    public void setFinalPriceOff(int finalPriceOff) {
        this.finalPriceOff = finalPriceOff;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public void setTestItemPrices(Map<String, Integer> testItemPrices) {
        this.testItemPrices = testItemPrices;
    }

}
