package com.asm.testit.models.ResponseModels;

import com.asm.testit.models.enumerations.BankPortal;



public class PaymentForm {
    int id;
    int price;
    BankPortal portal;

    public int getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }

    public BankPortal getPortal() {
        return portal;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setPortal(BankPortal portal) {
        this.portal = portal;
    }
}
