package com.asm.testit.models;

import java.util.*;
public class Sampler extends User {
    private int employeeNum;
    private List<Integer> samplerSlotIds;

    public Sampler(String name, String phoneNumber,String password, String nId, int employeeNum){
        super(name, phoneNumber, password, nId);
        this.employeeNum = employeeNum;
    }
}
