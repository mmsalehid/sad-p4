package com.asm.testit.models;

import com.asm.testit.models.ResponseModels.LabWithPriceInfo;
import com.asm.testit.models.enumerations.BloodGroup;
import com.asm.testit.models.enumerations.Gender;
import java.util.*;

public class Patient extends User {
    private BloodGroup bloodGroup;
    private Gender gender;
    private String address;
    private Test currentTest;
    private String insuranceName;

    public Patient(String name, String phoneNumber,String password, String nId, BloodGroup bloodGroup, Gender gender, String address, String insuranceName){
        super(name, phoneNumber, password, nId);
        this.bloodGroup = bloodGroup;
        this.gender = gender;
        this.address = address;
        this.insuranceName = insuranceName;
        currentTest = null;
    }

    public void initiateRequest(Test newTest){
        if (currentTest == null)
            currentTest = newTest;
        else{
            //TODO raise exception that cur test is running.
        }
        System.out.println("TEST");
    }

    public void setTestItems(List<TestItem> items){
        if (currentTest == null){
            throw new RuntimeException("ابتدا درخواست برای ایجاد آزمایش جدید دهید.");
        }
        currentTest.setItems(items);
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public boolean getCurrTestUseIns(){
        if (currentTest == null){
            throw new RuntimeException("ابتدا درخواست برای ایجاد آزمایش جدید دهید.");
        }
        return currentTest.isUseInsurance();
    }

    public void assignLabToCurrTest(Laboratory lab){
        if (currentTest == null){
            throw new RuntimeException("ابتدا درخواست برای ایجاد آزمایش جدید دهید.");
        }
        currentTest.assignLaboratory(lab);
    }

    public void setSlotForCurrentTest(SamplerSlot slot){
        if (currentTest == null){
            throw new RuntimeException("ابتدا درخواست برای ایجاد آزمایش جدید دهید.");
        }
        currentTest.setSamplerSlot(slot);
    }

    public void setInsuranceForCurrentTest(Insurance ins){
        if (currentTest == null){
            throw new RuntimeException("ابتدا درخواست برای ایجاد آزمایش جدید دهید.");
        }
        currentTest.setInsurance(ins);
    }

    public void calculatePriceForCurrentTest(){
        if (currentTest == null){
            throw new RuntimeException("ابتدا درخواست برای ایجاد آزمایش جدید دهید.");
        }
        currentTest.calculatePrice();
    }

    public LabWithPriceInfo getCurrentTestPriceInfo(){
        if (currentTest == null){
            throw new RuntimeException("ابتدا درخواست برای ایجاد آزمایش جدید دهید.");
        }
        return currentTest.getPriceInfo();
    }


    public void setPaymentForCurrentTest(Payment payment){
        if (currentTest == null){
            throw new RuntimeException("ابتدا درخواست برای ایجاد آزمایش جدید دهید.");
        }
        currentTest.setPayment(payment);
    }

    public void bookChosenSlot(){
        if (currentTest == null){
            throw new RuntimeException("ابتدا درخواست برای ایجاد آزمایش جدید دهید.");
        }
        currentTest.bookChosenSlot();
    }
}
